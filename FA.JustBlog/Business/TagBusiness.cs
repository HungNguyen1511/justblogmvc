﻿using FA.JustBlog.Core.Model;
using FA.JustBlog.Core.Service.RepositoryBase;
using FA.JustBlog.ModelMapping;
using FA.JustBlog.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FA.JustBlog.Business
{
    public class TagBusiness
    {
        public static JustBlogContext context = new JustBlogContext();

        public static TagRepository tagRepository = new TagRepository(context);

        public static List<TagViewModel> GetAllTag()
        {
            var allTag = tagRepository.GetAll();

            if (allTag.Count() > 0)
            {
                var listTagModels = new List<TagViewModel>();
                foreach (var tag in allTag)
                {
                    var tagModel = tag.ToModel();
                    listTagModels.Add(tagModel);
                }
                return listTagModels;
            }
            return null;
        }




    }
}