﻿using FA.JustBlog.Core.Model;
using FA.JustBlog.Core.Service.RepositoryBase;
using FA.JustBlog.ModelMapping;
using FA.JustBlog.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FA.JustBlog.Business
{
    public static class CategoryBusiness
    {
        public static JustBlogContext context = new JustBlogContext();

        public static CategoryRepository categoryRepository = new CategoryRepository(context);

        public static List<CategoryViewModel> GetAllCategory()
        {
            var allCategories = categoryRepository.GetAll();

            if (allCategories.Count() > 0)
            {
                var listCategoryModels = new List<CategoryViewModel>();
                foreach (var category in allCategories)
                {
                    var categoryModel = category.ToModel();
                    listCategoryModels.Add(categoryModel);
                }
                return listCategoryModels;
            }
            return null;
        }

    }
}