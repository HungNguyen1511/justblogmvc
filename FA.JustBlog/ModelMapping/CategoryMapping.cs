﻿using FA.JustBlog.Core.Model;
using FA.JustBlog.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FA.JustBlog.ModelMapping
{
    public static class CategoryMapping
    {
        public static CategoryViewModel ToModel(this Category category)
        {
            var viewModel = new CategoryViewModel()
            {
                CategoryId = category.CategoryId,
                Name = category.Name,
                UrlSlug = category.UrlSlug,
                Description = category.Description, 
            };

            if (category.Posts?.Count > 0)
            {
                foreach (Post post in category.Posts)
                {
                    viewModel.Posts.Add(post.ToModel());
                }
            }
            return viewModel;
        }
    }
}