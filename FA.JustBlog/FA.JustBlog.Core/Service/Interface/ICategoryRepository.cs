﻿using FA.JustBlog.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core.Service.Interface
{
    public interface ICategoryRepository : IRepositoryBase<Category>
    {
        
    }
}
