﻿using FA.JustBlog.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core.Service.Interface
{
    public interface ICommentRepository : IRepositoryBase<Comment>
    {
        IEnumerable<Comment> GetCommentsForPost(int postId);
        IEnumerable<Comment> GetCommentsForPost(Post post);
    }
}
