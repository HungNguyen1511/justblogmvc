﻿using FA.JustBlog.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core.Service.Interface
{
    public interface IPostRepository : IRepositoryBase<Post>
    {
        IEnumerable<Post> GetPublisedPosts();
        IEnumerable<Post> GetUnpublisedPosts();
        IEnumerable<Post> GetLatestPost(int size);
        IEnumerable<Post> GetPostsByMonth(DateTime monthYear);
        int CountPostsForCategory(string category);
        IEnumerable<Post> GetPostsByCategory(string category);
        int CountPostsForTag(string tag);
        IEnumerable<Post> GetPostsByTag(string tag);
        IEnumerable<Post> GetMostViewedPost(int size);
        IEnumerable<Post> GetHighestPosts(int size);
    }
}
