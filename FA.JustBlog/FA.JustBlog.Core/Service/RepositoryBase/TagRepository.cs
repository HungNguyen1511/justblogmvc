﻿using FA.JustBlog.Core.Model;
using FA.JustBlog.Core.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core.Service.RepositoryBase
{
    public class TagRepository : RepositoryBase<Tag>, ITagRepository
    {
        private readonly JustBlogContext _context;

        public TagRepository(JustBlogContext context) : base(context)
        {
            _context = context;
        }
        public Tag GetTagByUrlSlug(string urlSlug)
        {
            return _context.Tags.Where(x => x.UrlName.Contains(urlSlug)).FirstOrDefault();
        }
    }
}
