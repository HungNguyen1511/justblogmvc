﻿using FA.JustBlog.Core.Model;
using FA.JustBlog.Core.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core.Service.RepositoryBase
{
    public class CommentRepository : RepositoryBase<Comment>, ICommentRepository
    {
        private readonly JustBlogContext _context;

        public CommentRepository(JustBlogContext context) : base(context)
        {
            _context = context;
        }
        public IEnumerable<Comment> GetCommentsForPost(int postId)
        {
            return _context.Comments.Where(x => x.Post.PostId == postId).ToList();
        }
        public IEnumerable<Comment> GetCommentsForPost(Post post)
        {
            return _context.Comments.Where(x => x.Post.PostId == post.PostId).ToList();
        }
    }
}
