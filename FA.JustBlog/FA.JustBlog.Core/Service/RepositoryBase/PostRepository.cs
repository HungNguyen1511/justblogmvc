﻿using FA.JustBlog.Core.Model;
using FA.JustBlog.Core.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core.Service.RepositoryBase
{
    public class PostRepository : RepositoryBase<Post>, IPostRepository
    {
        private readonly JustBlogContext _context;

        public PostRepository(JustBlogContext context) : base(context)
        {
            _context = context;
        }

        public int CountPostsForCategory(string category)
        {
            return _context.Posts.Where(x => x.Category.Name.Contains(category)).Count();
        }

        public int CountPostsForTag(string tag)
        {
            return _context.Posts.Where(x => x.Tags.All(t => t.Name.Contains(tag))).Count();
        }   
        public IEnumerable<Post> GetHighestPosts(int size)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Post> GetLatestPost(int size)
        {
            return _context.Posts.OrderByDescending(x => x.Post_On).Take(size).ToList();
        }
        public IEnumerable<Post> GetMostViewedPost(int size)
        {
            return _context.Posts.OrderByDescending(x => x.ViewCount).Take(size).ToList();
        }

        public IEnumerable<Post> GetPostsByCategory(string category)
        {
            return _context.Posts.Where(x => x.Category.Name.Contains(category)).ToList();
        }

        public IEnumerable<Post> GetPostsByMonth(DateTime monthYear)
        {
            return _context.Posts.Where(x => x.Post_On.Month == monthYear.Month);
        }

        public IEnumerable<Post> GetPostsByTag(string tag)
        {
            return _context.Posts.Where(x => x.Tags.All(t => t.Name.Contains(tag))).ToList();
        }

        public IEnumerable<Post> GetPublisedPosts()
        {
            return _context.Posts.Where(x => x.Published).ToList();
        }

        public IEnumerable<Post> GetUnpublisedPosts()
        {
            return _context.Posts.Where(x => !x.Published).ToList();
        }
    }
}
