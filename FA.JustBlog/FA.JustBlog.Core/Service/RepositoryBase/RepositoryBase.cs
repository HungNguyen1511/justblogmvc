﻿using FA.JustBlog.Core.Model;
using FA.JustBlog.Core.Service.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core.Service.RepositoryBase
{
    public class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        private readonly JustBlogContext _db;
        internal DbSet<T> dbSet;

        public RepositoryBase(JustBlogContext db)
        {
            _db = db;
            this.dbSet = _db.Set<T>();
        }
        public virtual void Add(T entity)
        {
            dbSet.Add(entity);
        }
        public virtual void Delete(T entity)
        {
            dbSet.Remove(entity);
        }

        public virtual T Find(int id)
        {
            return dbSet.Find(id);
        }

        public virtual IEnumerable<T> GetAll()
        {
            return dbSet.AsEnumerable();
        }
        public virtual void Update(T entity)
        {
            dbSet.Attach(entity);
            _db.Entry(entity).State = EntityState.Modified;
        }
        public virtual void Commit()
        {
            _db.SaveChanges();
        }
    }
}
