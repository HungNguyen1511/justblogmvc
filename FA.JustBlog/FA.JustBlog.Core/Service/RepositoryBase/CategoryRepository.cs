﻿using FA.JustBlog.Core.Model;
using FA.JustBlog.Core.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core.Service.RepositoryBase
{
    public class CategoryRepository : RepositoryBase<Category>, ICategoryRepository
    {
        private readonly JustBlogContext _context;

        public CategoryRepository(JustBlogContext context) : base(context)
        {
            _context = context;
        }
    }
}
