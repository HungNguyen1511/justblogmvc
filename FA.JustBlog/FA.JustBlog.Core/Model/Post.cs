﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core.Model
{
    public class Post
    {
        public int PostId { set; get; }
        [Required(ErrorMessage = "Title name is required.")]
        [StringLength(255)]
        public string Title { set; get; }
        [StringLength(255)]
        public string Short_Desciption { set; get; }
        public string Post_Content { set; get; }
        [StringLength(255)]
        public string UrlSlug { set; get; }
        public bool Published { set; get; }
        public int ViewCount { set; get; }
        public int RateCount { set; get; }
        public int TotalRate { set; get; }
        public decimal Rate { get { return TotalRate / RateCount; } }
        public DateTime Post_On { set; get; }
        public DateTime Modified { set; get; }
        public int CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public virtual Category Category { set; get; }
        public virtual ICollection<Tag> Tags { get; set; }
        public virtual ICollection<Comment> Comments { set; get; }
    }
}
