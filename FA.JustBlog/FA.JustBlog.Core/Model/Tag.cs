﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core.Model
{
    public class Tag
    {
        public int TagId { set; get; }
        public string Name { set; get; }
        public string UrlName { set; get;}
        public string Description { set; get; }
        public int Count { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}
