﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core.Model
{
    public class Category
    {
        public int CategoryId { set; get; }
        [Required(ErrorMessage = "Category name is required.")]
        [StringLength(255)]
        public string Name { set; get; }
        [StringLength(255)]
        public string UrlSlug { set; get; }
        [StringLength(1024)]
        public string Description { set; get; }
        public virtual List<Post> Posts { set; get; }
    }
}
