﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core.Model
{
    public class JustBlogContext : DbContext
    {
        public JustBlogContext() : base("name=JustBlog")
        {
            Database.SetInitializer<JustBlogContext>(null);
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Post>()
                .HasMany<Tag>(s => s.Tags)
                .WithMany(c => c.Posts)
                .Map(cs =>
                {
                    cs.MapLeftKey("PostId");
                    cs.MapRightKey("TagId");
                    cs.ToTable("PostTagMap");
                });
        }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Comment> Comments { get; set; }
    }
}
