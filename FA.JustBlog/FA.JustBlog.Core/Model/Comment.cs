﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core.Model
{
    public class Comment
    {
        public int CommentId { set; get; }
        public string Name { set; get; }
        public string Email { set; get; }
        public string CommentHeader { set; get; }
        public string CommentText { set; get; }
        public DateTime CommentTime { set; get; }
        public int PostId { set; get; }
        public virtual Post Post { set; get; }
    }
}
