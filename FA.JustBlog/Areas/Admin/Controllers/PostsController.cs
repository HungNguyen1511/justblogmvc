﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FA.JustBlog.Business;
using FA.JustBlog.Core.Model;
using FA.JustBlog.CustomHelper;
using FA.JustBlog.ViewModels;

namespace FA.JustBlog.Areas.Admin.Controllers
{
    public class PostsController : Controller
    {
        private JustBlogContext db = new JustBlogContext();

        // GET: Admin/Posts
        public ActionResult Index()
        {
            var posts = PostBusiness.GetAllPost();    
            return View(posts);
        }

        // GET: Admin/Posts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = db.Posts.Find(id);

            if (post == null)
            {
                return HttpNotFound();
            }
            

            return View(post);
        }

        // GET: Admin/Posts/Create
        public ActionResult Create()
        {
            ViewBag.CategoryId = new SelectList(CategoryBusiness.GetAllCategory(), "CategoryId", "Name");
            return View();
        }


        // POST: Admin/Posts/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PostId,Title,Short_Desciption,Post_Content,Published,TotalRate,Modified,RateCount,CategoryId")] Post post)
        {
            if (ModelState.IsValid)
            {
                post.Post_On = DateTime.Now;
                post.ViewCount = 0;
                post.UrlSlug = GenerateUrlSlug.UrlFriendly(post.Title);
                PostBusiness.Add(post);
                return RedirectToAction("Index");
            }

            ViewBag.CategoryId = new SelectList(db.Categories, "CategoryId", "Name", post.CategoryId);
            return View(post);
        }

        // GET: Admin/Posts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryId = new SelectList(db.Categories, "CategoryId", "Name", post.CategoryId);
            return View(post);
        }

        // POST: Admin/Posts/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PostId,Title,Short_Desciption,Post_Content,UrlSlug,Published,ViewCount,RateCount,TotalRate,Post_On,Modified,CategoryId")] Post post)
        {
            if (ModelState.IsValid)
            {
                db.Entry(post).State = EntityState.Modified;
                post.Modified = DateTime.Now;
                post.Post_On = DateTime.Now;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryId = new SelectList(db.Categories, "CategoryId", "Name", post.CategoryId);
            return View(post);
        }

        // GET: Admin/Posts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        // POST: Admin/Posts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult LastestPosts()
        {
            var allLastestPost = PostBusiness.GetLastestPost(5);
            return View("Index", allLastestPost.ToList());
        }
        public ActionResult MostViewedPosts()
        {
            var allMostViewPost = PostBusiness.GetMostViewPost(5);
            return View("Index", allMostViewPost.ToList());
        }
        public ActionResult PublishedPost()
        {
            var allPublishedPost = PostBusiness.GetPublishedPost();
            return View("Index", allPublishedPost.ToList());
        }
        public ActionResult UnpublishedPost()
        {
            var allUnpublishedPost = PostBusiness.GetUnPubLishedPost();
            return View("Index", allUnpublishedPost.ToList());
        }





        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
