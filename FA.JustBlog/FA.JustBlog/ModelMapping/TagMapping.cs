﻿using FA.JustBlog.Core.Model;
using FA.JustBlog.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FA.JustBlog.ModelMapping
{
    public static class TagMapping
    {

        public static TagViewModel ToModel(this Tag tag)
        {
            var postViewModel = new TagViewModel()
            {
                TagId = tag.TagId,
                Description = tag.Description,
                Name = tag.Name,
                Count = tag.Count,
            };
            return postViewModel;
        }
    }
}