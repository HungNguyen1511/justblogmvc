﻿using FA.JustBlog.Core.Model;
using FA.JustBlog.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FA.JustBlog.ModelMapping
{
    public static class PostMapping
    {
        public static PostViewModel ToModel(this Post post)
        {
            var postViewModel = new PostViewModel()
            {
                PostId = post.PostId,
                Title = post.Title,
                Short_Desciption = post.Short_Desciption,
                CategoryName = post.Category?.Name,
                Post_Content = post.Post_Content,
                RateCount = post.RateCount,
                TotalRate = post.TotalRate,
                ViewCount = post.ViewCount,
                Post_On = post.Post_On,
                Modified = post.Modified
            };
            return postViewModel;
        }

    }
}