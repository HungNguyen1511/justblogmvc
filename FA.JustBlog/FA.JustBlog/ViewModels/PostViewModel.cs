﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FA.JustBlog.ViewModels
{
    public class PostViewModel
    {
        public PostViewModel()
        {
            RelativePost = new List<PostViewModel>();
            RelativeTag = new List<TagViewModel>();
        }
        public int PostId { set; get; }
        public string Title { set; get; }
        public string Short_Desciption { set; get; }
        public string CategoryName { get; set; }
        public int ViewCount { set; get; }
        public int RateCount { set; get; }
        public int TotalRate { set; get; }
        public string Post_Content { set; get; }
        public string UrlSlug { set; get; }
        public bool Published { set; get; }
        public DateTime Post_On { set; get; }
        public DateTime Modified { set; get; }
        public List<PostViewModel> RelativePost { get; set; }
        public List<TagViewModel> RelativeTag { set; get; }
    }
}