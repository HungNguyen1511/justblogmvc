﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FA.JustBlog.ViewModels
{
    public class CategoryViewModel
    {
        public CategoryViewModel()
        {
            Posts = new List<PostViewModel>();
        }
        public int CategoryId { set; get; }
        public string Name { set; get; }
        public string UrlSlug { set; get; }
        public string Description { set; get; }
        public virtual List<PostViewModel> Posts { get; set; }
    }
}