﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FA.JustBlog.ViewModels
{
    public class TagViewModel
    {
        public int TagId { set; get; }
        public string Name { set; get; }
        public string UrlName { set; get; }
        public string Description { set; get; }
        public int Count { set; get; } 
    }
}