﻿using FA.JustBlog.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FA.JustBlog.Controllers
{
    public class CategoriesController : Controller
    {
        // GET: Category
        public ActionResult Index()
        {
            var allCategory = CategoryBusiness.GetAllCategory();
            // trả về view hiển thị ra toàn bộ danh sách của category
            return View();
        }

        public ActionResult GetCategoryForMenu()
        {
            var allCategoryMenu = CategoryBusiness.GetAllCategory();
            // trả về view hiển thị ra toàn bộ danh sách của category
            return PartialView("_PartialMenuCategory", allCategoryMenu.ToList());
        }
    }
}