﻿using FA.JustBlog.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FA.JustBlog.Controllers
{
    public class PostController : Controller
    {
        // GET: Post
        public ActionResult Index()
        {
            var allPost = PostBusiness.GetAllPost();
            // trả về view hiển thị ra toàn bộ danh sách của post
            return View(allPost);
        }
       
        public ActionResult Detail(int id)
        {
            var post = PostBusiness.GetDetail(id);
            return View("DetailPost", post);     
        }

        [ChildActionOnly]
        public ActionResult LastestPosts()
        {
            var allLastestPost = PostBusiness.GetLastestPost(5);
            return PartialView("_ListPost", allLastestPost.ToList());
        }

        [ChildActionOnly]
        public ActionResult MostViewedPosts()
        {
            var allLastestPost = PostBusiness.GetMostViewPost(5);
            return PartialView("_ListPost", allLastestPost.ToList());
        }
    }
}