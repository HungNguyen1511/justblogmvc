﻿using FA.JustBlog.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FA.JustBlog.Controllers
{
    public class CategoryController : Controller
    {
        // GET: Category
        public ActionResult Index()
        {
            var allPost = CategoryBusiness.GetAllCategory();
            // trả về view hiển thị ra toàn bộ danh sách của category
            return View();
        }
    }
}