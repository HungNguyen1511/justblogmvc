﻿using FA.JustBlog.Core.Model;
using FA.JustBlog.Core.Service.RepositoryBase;
using FA.JustBlog.ModelMapping;
using FA.JustBlog.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FA.JustBlog.Business
{
    public class PostBusiness
    {
        public static JustBlogContext context = new JustBlogContext();

        public static PostRepository postRepository = new PostRepository(context);
        public static List<PostViewModel> GetAllPost()
        {
            var allpost = postRepository.GetAll();

            if (allpost.Count() > 0)
            {
                var listPostModels = new List<PostViewModel>();
                foreach (var post in allpost)
                {
                    var postModel = post.ToModel();
                    listPostModels.Add(postModel);
                }
                return listPostModels;
            }
            return null;
        }
        
        public static List<PostViewModel> GetLastestPost(int size)
        {
            var ListLastestPost = postRepository.GetLatestPost(size);

            if (ListLastestPost.Count() > 0)
            {
                var listPostModels = new List<PostViewModel>();
                foreach (var post in ListLastestPost)
                {
                    var postModel = post.ToModel();
                    listPostModels.Add(postModel);
                }
                return listPostModels;
            }
            return null;
        }

        public static List<PostViewModel> GetMostViewPost(int size)
        {
            var ListMostViewPost = postRepository.GetMostViewedPost(size);

            if (ListMostViewPost.Count() > 0)
            {
                var listPostModels = new List<PostViewModel>();
                foreach (var post in ListMostViewPost)
                {
                    var postModel = post.ToModel();
                    listPostModels.Add(postModel);
                }
                return listPostModels;
            }
            return null;
        }

        public static void Add(Post post)
        {
            postRepository.Add(post);
            postRepository.Commit();
        }
       
        public static PostViewModel GetDetail(int id)
        {
            
            var post =  postRepository.Find(id).ToModel();
            if (post != null)
            {
                return post;
            }
            else
            {
                return null;
            }
        }

        public static List<PostViewModel> GetPostByCategory(string CategoryName)
        {
            var ListPost = postRepository.GetPostsByCategory(CategoryName);

            if (ListPost.Count() > 0)
            {
                var listPostModels = new List<PostViewModel>();
                foreach (var post in ListPost)
                {
                    var postModel = post.ToModel();
                    listPostModels.Add(postModel);
                }
                return listPostModels;
            }
            return null;
        }
    }
}