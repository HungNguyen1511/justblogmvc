﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FA.JustBlog
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Tag",
                url: "Tag/{tagName}",
                defaults: new { controller = "Posts", action = "GetPostByTag" },
                namespaces: new[] { "FA.JustBlog.Controllers" }
            );

            routes.MapRoute(
                name:"Post",
                url: "Post/{year}/{month}/{urlSlug}",
                defaults: new { controller = "Posts", action = "GetPostByDateAndTitle" },
                constraints: new { year = @"\d{4}", month = @"\d{2}" },
                namespaces: new[] { "FA.JustBlog.Controllers" }
            );
            

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Posts", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "FA.JustBlog.Controllers" }
            );

            
        }
    }
}
