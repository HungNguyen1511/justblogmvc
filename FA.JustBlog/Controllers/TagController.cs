﻿using FA.JustBlog.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FA.JustBlog.Controllers
{
    public class TagController : Controller
    {
        // GET: Tag
        public ActionResult GetAllTag()
        {
            var getAllTag = TagBusiness.GetAllTag();

            return PartialView("_PartialTag", getAllTag.ToList());
        }
    }
}