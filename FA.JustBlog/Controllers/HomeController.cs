﻿using FA.JustBlog.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FA.JustBlog.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [ChildActionOnly]
        public ActionResult AboutCard()
        {
            return PartialView("_PartialAboutCard");
        }


        public ActionResult MostViewedPosts()
        {
            var allLastestPost = PostBusiness.GetMostViewPost(5);
            return PartialView("_ListPost", allLastestPost.ToList());
        }

        public ActionResult LastestPost()
        {
            var allLastestPost = PostBusiness.GetLastestPost(5);
            return PartialView("_ListPost", allLastestPost.ToList());
        }
    }
}