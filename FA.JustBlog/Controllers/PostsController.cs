﻿using FA.JustBlog.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FA.JustBlog.Controllers
{
    public class PostsController : Controller
    {
        // GET: Post
        public ActionResult Index()
        {
            var allPost = PostBusiness.GetAllPost();
            // trả về view hiển thị ra toàn bộ danh sách của post
            return View(allPost);
        }
       
        public ActionResult Detail(int id)
        {
            var post = PostBusiness.GetDetail(id);
            PostBusiness.Update(id);
            return View(post);     
        }

        [ChildActionOnly]
        public ActionResult LastestPosts()
        {
            var allLastestPost = PostBusiness.GetLastestPost(5);
            return PartialView("_ListPost", allLastestPost.ToList());
        }

        [ChildActionOnly]
        public ActionResult MostViewedPosts()
        {
            var allLastestPost = PostBusiness.GetMostViewPost(5);
            return PartialView("_ListPost", allLastestPost.ToList());
        }
        
        public ActionResult GetPostByCategory(string id)
        {
            var allPostByCategory = PostBusiness.GetPostByCategory(id);
            return View("Index", allPostByCategory);
        }

        public ActionResult GetPostByDateAndTitle(int year, int month, string urlSlug)
        {
            
            var post = PostBusiness.GetPostByDateAndTitle(year, month, urlSlug);
            if (post == null)
                return HttpNotFound();
            int id = post.PostId;
            PostBusiness.Update(id);
            return View("Detail", post);
        }

        public ActionResult GetPostByTag(string tagName)
        {
            var listPost = PostBusiness.GetPostByTag(tagName);
            return View("Index", listPost);
        }
    }
}